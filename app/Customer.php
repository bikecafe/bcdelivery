<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'customers';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	public function addresses()
    {
        return $this->hasMany('App\CustomerAddress', 'customer_id');
    }

    public function contacts()
    {
        return $this->hasMany('App\CustomerContact', 'customer_id');
    }

    public function getLatestContact(){
		return $this->contacts()->orderBy('created_at', 'desc')->first();
	}

	public function getPrimaryAddress(){
		return count($this->addresses()->where('is_primary',1)->first()) > 0 ? $this->addresses()->where('is_primary',1)->first() : $this->addresses()->orderBy('created_at','desc')->first();
	}

	public function orders(){
		return $this->hasMany('App\Order', 'customer_id');
	}

	public function ordersDescending(){
		return $this->orders()->orderBy('created_at', 'desc')->get();
	}

}
