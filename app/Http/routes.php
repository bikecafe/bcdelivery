<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
    'as' => 'home', 'uses' => 'HomeController@index'
]);
Route::post('/contact', 'HomeController@contact');
Route::get('/about', 'HomeController@about');
Route::get('/login', 'UsersController@login');
Route::post('/login', 'UsersController@postLogin');
Route::post('/validate/username', 'UsersController@validateUsername');
Route::post('/validate/email', 'UsersController@validateEmail');
Route::post('/validate/{id}/email', 'UsersController@validateOtherEmail');
Route::get('/register', 'UsersController@register');
Route::post('/register', 'UsersController@postRegister');
Route::get('/registered', 'UsersController@registered');
Route::get('/activation', 'UsersController@activation');
Route::post('/activation', 'UsersController@postActivation');
Route::get('/verification', 'UsersController@verification');
Route::post('/verification', 'UsersController@postVerification');
Route::get('/reset', 'UsersController@reset');
Route::post('/reset', 'UsersController@postReset');
Route::get('/reset/password', 'UsersController@password');
Route::post('/reset/password', 'UsersController@postPassword');
Route::get('/reset/{user}/{email}/{token}', [
    'as' => 'reset', 'uses' => 'UsersController@resetPassword'
]);
Route::get('/activate/{user}/{email}/{token}', [
    'as' => 'activate', 'uses' => 'UsersController@activate'
]);

Route::get('/email/validate/{user}/{email}/{token}', [
    'as' => 'validate', 'uses' => 'UsersController@validateNewEmail'
]);
Route::get('/success', ['as' => 'success', 'uses' => 'MiscController@show']);
Route::get('/tray', 'OrdersController@index');
Route::post('/tray/add', 'OrdersController@addToTray');
Route::get('/tray/count', 'OrdersController@countOrders');
Route::get('/tray/clear', 'OrdersController@clearOrders');
Route::get('/tray/cost', 'OrdersController@getOrdersCost');
Route::get('/tray/{id}/clear', 'OrdersController@clearOrder');
Route::get('/tray/{id}/quantity/{quantity}', 'OrdersController@updateOrderQuantity');

Route::group(['middleware' => 'authorize'],function(){
	Route::get('/account', 'UsersController@account');
	Route::post('/account', 'UsersController@postAccount');
	Route::post('/account/address', 'UsersController@addAddress');
	Route::get('/account/{id}/address', 'UsersController@getAddressList');
	Route::get('/account/{id}/address/latest', 'UsersController@getAddressLatest');
	Route::post('/account/password', 'UsersController@updatePassword');
	Route::post('/account/{id}/password/verify', 'UsersController@verifyPassword');
	Route::post('/tray', 'OrdersController@checkoutOrders');
	Route::get('/logout', 'UsersController@logout');
});

Route::get('/menu', 'ProductsController@index');

Route::get('/registered/success', function()
{
    return view('messages.registered_success');
});
Route::get('/registered/error', function()
{
    return view('messages.registered_error');
});

Route::get('/activated/success', function()
{
    return view('messages.activated_success');
});

Route::get('/activated/expired', function()
{
    return view('messages.activated_expired');
});

Route::get('/activated/error', function()
{
    return view('messages.activated_error');
});

Route::get('/activation/success', function()
{
    return view('messages.activation_success');
});

Route::get('/reset/success', function()
{
    return view('messages.reset_success');
});

Route::get('/reset/expired', function()
{
    return view('messages.reset_expired');
});

Route::get('/reset/error', function()
{
    return view('messages.reset_error');
});

Route::get('/verified/success', function()
{
    return view('messages.verified_success');
});

Route::get('/verified/expired', function()
{
    return view('messages.verified_expired');
});

Route::get('/verification/success', function()
{
    return view('messages.verification_success');
});

Route::get('/password/success', function()
{
    return view('messages.password_success');
});

Route::get('/password/error', function()
{
    return view('messages.password_error');
});

Route::get('/orders/success', function()
{
    return view('messages.ordersent_success');
});

Route::get('/orders/expired', function()
{
    return view('messages.ordersent_expired');
});