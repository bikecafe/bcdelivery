<?php namespace App\Http\Middleware;

use Closure;
use App\Customer;
use Illuminate\Http\RedirectResponse;

class Authorize {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$id = session('curr_id');
		$customer = Customer::find($id);
		if(count($customer)>0){
			return $next($request);
		} else {
			return redirect('/login');
		}
		
	}

}
