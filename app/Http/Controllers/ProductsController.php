<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Category;
use App\Customer;
use App\Product;
use App\ProductCategory;
use Cart;

class ProductsController extends Controller {

	public function index()
	{
		//get currently logged in user
		$user = Session::get('curr_id');
		$customer = Customer::find($user);

		$productList = array();
		$categories = Category::where(['is_shown'=>1,'is_deleted'=>0])->orderby('rank','asc')->get();
		foreach($categories as $category){
			$productList[$category->id] = array(
				'id' => $category->id,
				'name' => $category->name,
				'rank' => $category->rank
				);
			$products = ProductCategory::where('category_id',$category->id)->orderby('rank','asc')->get();
			$productList[$category->id]['products'] = array();
			foreach($products as $product){
				$product_info = Product::where(['id'=>$product->id,'is_deleted'=>0])->first();
				if(count($product_info)>0){
					$productList[$category->id]['products'][$product->id] = array(
						'id' => $product_info->id,
						'name' => $product_info->name,
						'price' => $product_info->price,
						'description' => $product_info->description,
						'image' => $product_info->image,
						'rank' => $product_info->rank
					); 
				}
				
			}
		}
		return view('menu.index', compact('productList','customer'));
	}


}
