<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Category;
use App\Customer;
use App\Product;
use App\ProductCategory;
use App\Order;
use App\OrderLine;
use App\OrderWorkflow;
use Cart;
use Session;

class OrdersController extends Controller {

	public function index()
	{
		//get currently logged in user
		$user = Session::get('curr_id');
		$customer = Customer::find($user);
		if(!Cart::isEmpty()){
			$orders = Cart::getContent();
			$orders = $orders->toArray();
		} else {
			$orders = [];
		}
		return view('orders.index',compact('orders','customer'));
	}

	public function addToTray(){
		$id = Input::get('id');
		$quantity = Input::get('quantity');
		$product = Product::find($id);
		//check if product exists
		if(count($product)>0){
			Cart::add($product->id, $product->name, $product->price, $quantity, array());
			if(!Cart::isEmpty()){
				echo json_encode(array('status'=>1,'added'=>'true','id'=>$id,'quantity'=>$quantity));
			} else {
				echo json_encode(array('status'=>2,'added'=>'false','id'=>$id,'quantity'=>$quantity));
			}
			
		} else {
			echo json_encode(array('status'=>3,'added'=>'false','id'=>$id,'quantity'=>$quantity));
		}
	}

	public function countOrders(){
		$cartCollection = Cart::getContent();
		echo count($cartCollection);
	}

	public function clearOrder($id){
		Cart::remove($id);
		if(empty(Cart::get($id))){
			echo "order removed";
		} else {
			echo "order not removed";
		}
	}

	public function clearOrders(){
		if(!Cart::isEmpty()){
			Cart::clear();
			echo "cleared";
		} else {
			echo "nothing to clear";
		}
	}

	public function getOrdersCost(){
		echo Cart::getTotal();
	}

	public function updateOrderQuantity($id,$quantity){
		Cart::update($id, array(
		  'quantity' => array(
		      'relative' => false,
		      'value' => $quantity
		  ),
		));
		echo Cart::get($id)->getPriceSum();
	}

	public function checkoutOrders(){
		//get currently logged in user
		$user = Session::get('curr_id');
		$customer = Customer::find($user);
		if(!Cart::isEmpty() && !empty($customer)){
			$changeFor = Input::get('change_for');
	        $grandTotal = Cart::getTotal();
	        $orderStatus = 1; //pending
	        $referenceNo = time();
	        $acquisition = Input::get('acquisition');
	        $remarks = Input::get('comments');

			$products = Cart::getContent();
			$products = $products->toArray();
			$orderlines = array();
	        foreach($products as $product){
	            $orderline = new OrderLine;
	            $orderline->product_id = $product['id'];
	            $orderline->price = $product['price'];
	            $orderline->quantity = $product['quantity'];
	            $orderlines[] = $orderline;
	        }

	        $order = new Order;
	        $order->reference_no = $referenceNo;
	        $order->order_status_id = $orderStatus;
	        $order->customer_id = $user;
	        $order->total_price = $grandTotal;
	        $order->change_for = $changeFor;
	        $order->order_type_id = 2; //online
	        $order->order_acquisition_id = $acquisition;
	        if($acquisition == 2) //if order is for delivery, associate address
	        {
	        	$order->customer_address_id = $customer->getPrimaryAddress()->id;
	        }
	        $order->customer_contact_id = $customer->getLatestContact()->id;
	        $order->remarks = $remarks;

	        $order->save();

            if($order->id){
                //save order lines
                $order->orderlines()->saveMany($orderlines);

                //log order workflow (status + logged in customer who initiated the order)
                $workflow = new OrderWorkflow;
                $workflow->user_id = 1; //default to admin account
                $workflow->notes = "Online order";
                $workflow->order_status_id = $orderStatus;
                $order->workflow()->save($workflow);

                //clear tray
                Cart::clear();
                
				return redirect('/orders/success');
            }
			
		} else {
			return redirect('/orders/expired');
		}
	}

}
