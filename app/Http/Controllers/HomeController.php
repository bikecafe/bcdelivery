<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Libraries\Helpers;
use App\Product;
use Mail;
use App\Customer;

class HomeController extends Controller {


	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		//get currently logged in user
		$user = Session::get('curr_id');
		$customer = Customer::find($user);
		$bestSellers = Product::where(['id'=>60])->orWhere(['id'=>61])->orWhere(['id'=>62])->get();
		$entrees = Product::where(['id'=>49])->orWhere(['id'=>51])->orWhere(['id'=>52])->get();
		return view('home',compact('bestSellers','entrees','customer'));
	}

	public function about(){
		//get currently logged in user
		$user = Session::get('curr_id');
		$customer = Customer::find($user);
		return view('about',compact('customer'));
	}

	public function contact(){
		$contactName = Input::get('contact_name');
		$contactEmail = Input::get('contact_email');
		$contactMessage = Input::get('contact_message');
		$input = Input::all();
		
		if($input){
			$data = array('email'=>Helpers::getSettingValue('contact_email'), 'name'=> $contactName );

			Mail::send('emails.contact', ['contact_name' => $contactName, 'contact_email'=> $contactEmail, 'contact_message' => $contactMessage], function($message) use ($data)
			{
				$message->from('r88bikecafe@gmail.com', 'Route 88 Bike Café');
				$message->to($data['email'], 'Route 88 Bike Café')->subject('Contact Form - '.$data['name']);
			});
			Session::put('success','Success!');
			Session::put('message','Your message has been sent. Please wait for a response from our staff.');
			
			return redirect('/success');
		} else {
			return redirect('/');
		}
	}

}
