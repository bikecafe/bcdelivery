<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Customer;
use App\CustomerAddress;
use App\CustomerContact;
use App\Order;
use Mail;
use App\Libraries\Helpers;
use Validator;
use Cart;

class UsersController extends Controller {


	/**
	 * 
	 *
	 * @return Response
	 */
	public function login()
	{
		return view('users.login');
	}

	public function postLogin()
	{
		$username = Input::get('username');
		$password = Input::get('password');
		$customer = Customer::where(['username'=>$username, 'password'=>md5($password)])->first();
		//if login details exist and user is activated
		if(count($customer)>0 && $customer->is_activated == 1){
			//add session variable
			session(['curr_id' => $customer->id]);
			//redirect to homepage
			return redirect("/");
		} 
		//if login details exist but user is not activated 
		elseif(count($customer)>0 && $customer->is_activated == 0){
			return redirect("/login/")
					->withInput()
                    ->with('error','<p style="margin-left: 10px;color: #FF0000;">Account is not yet activated. Please click <a href="/activation">here</a> to re-send an activation link to your e-mail.</p>');
		}
		//if login details do not exist
		else {
			return redirect("/login/")
					->withInput()
                    ->with('error','<p style="margin-left: 10px;color: #FF0000;">Invalid login credentials</p>');
		}

	}

	public function register()
	{
		$home = route('home');
		$delivery_area = Helpers::getSettingValue('delivery_places');
		$delivery_area = explode(',',$delivery_area);
		return view('users.register', compact('home','delivery_area'));
	}

	public function postRegister(){
		$firstname = Input::get('firstname');
		$lastname = Input::get('lastname');
		$email = Input::get('email');
		$contactNumber = Input::get('contact');
		$address_house = Input::get('delivery_house');
		$address_street = Input::get('delivery_street');
		$address_area = Input::get('delivery_area');
		$username = Input::get('username');
		$password = Input::get('password');
		$acquisition_type_id = Input::get('acquisition');

		$input = Input::all();

		$customer = new Customer;
		$customer->firstname = $firstname;
		$customer->lastname = $lastname;
		$customer->email = $email;
		$customer->username = $username;
		$customer->password = md5($password);
		$customer->is_email_verified = 0;
		$customer->is_activated = 0;

		$customer->save();

		if($customer->id){
			$id = $customer->id;
			
			if($acquisition_type_id == 2){
				$address = new CustomerAddress;
				$address->customer_id = $id;
				$address->house_bldg = $address_house;
				$address->street = $address_street;
				$address->village_brgy = $address_area;
				$address->city = "Quezon City";
				$address->is_primary = 1;
				$address->save();
			}

			$contact = new CustomerContact;
			$contact->customer_id = $id;
			$contact->contact_number = $contactNumber;
			$contact->is_primary = 1;
			$contact->save();

			$token = time();
			$user = $customer->id;
			$email = md5($customer->email);
			$confirm_url = route('activate', compact('user','email','token')); 
			$data = array('email'=>$customer->email, 'name'=> $customer->firstname." ".$customer->lastname );

			Mail::send('emails.activate', ['confirm_url' => $confirm_url, 'username'=> $customer->username], function($message) use ($data)
			{
				$message->from('r88bikecafe@gmail.com', 'Route 88 Bike Café');
				$message->to($data['email'], $data['name'])->subject('Welcome to Bike Café!');
			});
			return redirect("/registered/success");
		}

		return redirect("/registered/error");
	}

	public function registered(){
		return view('users.registered');
	}

	public function account(){
		//get currently logged in user
		$user = Session::get('curr_id');
		$customer = Customer::find($user);
		$delivery_area = Helpers::getSettingValue('delivery_places');
		$delivery_area = explode(',',$delivery_area);
		return view('users.account',compact('customer','delivery_area'));
	}

	public function postAccount(){
		//PROFILE UPDATE

		$user = Session::get('curr_id');
		$customer = Customer::find($user);
		//if customer exists
		if(count($customer)>0){
			//get form input
			$firstname = Input::get('firstname');
			$lastname = Input::get('lastname');
			$email = Input::get('email');
			$contact = Input::get('contact');
			$address = Input::get('primary_address');

			$input = Input::all();

	        $rules = array(
	            'firstname' => 'required',
	            'lastname' => 'required',
	            'email' => 'required',
	            'contact' => 'required',
	            'primary_address' => 'required'
	        );

	        $messages = array(
	        	'firstname.required' => 'Please enter your first name',
	            'lastname.required' => 'Please enter your last name',
	            'email.required' => 'Please enter your e-mail address',
	            'contact.required' => 'Please enter your contact number',
	            'primary_address.required' => 'Please select your primary delivery address'
	        );

	        $validation = Validator::make($input, $rules, $messages);

	        if($validation->passes()){
				//determined updated fields
				$up_firstname = trim($firstname) == $customer->firstname ? false : true;
				$up_lastname = trim($lastname) == $customer->lastname ? false : true;
				$up_email = trim($email) == $customer->email ? false : true;
				$up_contact = trim($contact) == $customer->getLatestContact()->contact_number ? false : true;
				$up_address = trim($address) == $customer->getPrimaryAddress()->id ? false : true;

				//check if atleast one field has been updated
				if($up_firstname || $up_lastname || $up_email || $up_contact || $up_address){
					$customer->firstname = trim($firstname);
					$customer->lastname = trim($lastname);
					$customer->email = trim($email);
					
					//if email was updated
					if($up_email){
						//flag as email not verified
						$customer->is_email_verified = 0;

						//send validation e-mail to confirm new email
						$token = time();
						$user = $customer->id;
						$email = md5($customer->email);
						$confirm_url = route('validate', compact('user','email','token')); 
						$data = array('email'=>$customer->email, 'name'=> $customer->firstname." ".$customer->lastname );

						Mail::send('emails.validate', ['confirm_url' => $confirm_url, 'username'=> $customer->username], function($message) use ($data)
						{
							$message->from('r88bikecafe@gmail.com', 'Route 88 Bike Café');
							$message->to($data['email'], $data['name'])->subject('Route 88 Bike Café - Validate Your New E-mail Address');
						});
					}

					//if contact number was updated
					if($up_contact){
						//unset current primary contact
						$customer->contacts()->where(['is_primary'=>1])->update(['is_primary' => 0]);
						//save new contact number and set as primary
						$new_contact = new CustomerContact;
						$new_contact->contact_number = trim($contact);
						$new_contact->is_primary = 1;
						$customer->contacts()->save($new_contact);
					}

					//if primary address was changed
					if($up_address){
						//unset current primary contact
						$customer->addresses()->where(['is_primary'=>1])->update(['is_primary' => 0]);
						//set new primary address
						$customer->addresses()->where(['id'=>trim($address)])->update(['is_primary'=> 1]);
					}

					//save new customer details
					$customer->save();
					return redirect('/account')
					->with('flash_message','Profile Updated Successfully!');
				} else {
					//reload account page
					return redirect('/account')
						->with('flash_message','Nothing to update!');
				}
				
			} else {
				return redirect("/account")
                    ->withInput()
                    ->withErrors($validation);
			}
		}
	}

	public function logout(){
		Cart::clear();
		Session::flush();
		return redirect('/');
	}

	public function activation(){
		return view('users.activation');
	}

	public function postActivation(){
		$email = Input::get('email');
		$customer = Customer::where(['email'=>$email,'is_activated'=>0])->first();
		if(count($customer)>0){
			$token = time();
			$user = $customer->id;
			$email = md5($customer->email);
			$confirm_url = route('activate', compact('user','email','token')); 
			$data = array('email'=>$customer->email, 'name'=> $customer->firstname." ".$customer->lastname );
			Mail::send('emails.activate', ['confirm_url' => $confirm_url, 'username'=> $customer->username], function($message) use ($data)
			{
				$message->from('r88bikecafe@gmail.com', 'Route 88 Bike Café');
				$message->to($data['email'], $data['name'])->subject('Route 88 Bike Café - Account Activation');
			});
			
		}

		return redirect("/activation/success");
	}

	public function activate($user, $email, $token){
		$now = time();
		if(($now-$token) < 86400){ //check if link has been generated within the last 24 hours
			$customer = Customer::find($user);
			$customer->is_email_verified = 1;
			$customer->is_activated = 1;
			$customer->save();
			return redirect("/activated/success");
		} else {
			return redirect("/activated/expired");
		}
	}

	public function verification(){
		return view('users.verification');
	}

	public function postVerification(){
		$email = Input::get('email');
		$customer = Customer::where(['email'=>$email,'is_activated'=>0])->first();
		if(count($customer)>0){
			$token = time();
			$user = $customer->id;
			$email = md5($customer->email);
			$confirm_url = route('validate', compact('user','email','token')); 
			$data = array('email'=>$customer->email, 'name'=> $customer->firstname." ".$customer->lastname );

			Mail::send('emails.validate', ['confirm_url' => $confirm_url, 'username'=> $customer->username], function($message) use ($data)
			{
				$message->from('r88bikecafe@gmail.com', 'Route 88 Bike Café');
				$message->to($data['email'], $data['name'])->subject('Route 88 Bike Café - Validate Your New E-mail Address');
			});
		}
		return redirect("/verification/success");
	}

	public function validateNewEmail($user, $email, $token){
		$now = time();
		if(($now-$token) < 86400){ //check if link has been generated within the last 24 hours
			$customer = Customer::find($user);
			$customer->is_email_verified = 1;
			$customer->save();
			return redirect("/verified/success");
		} else {
			return redirect('/verified/expired');
		}
	}

	public function reset(){
		return view('users.reset');
	}

	public function postReset(){
		$email_uname = Input::get('email');
		//check if field input is e-mail
		$c_email = Customer::where(['email'=>$email_uname])->first();
		$c_uname = Customer::where(['username'=>$email_uname])->first();
		$customer = !empty($c_email) ? $c_email : (!empty($c_uname) ? $c_uname: NULL);
		
		if(!empty($customer)){
			$token = time();
			$user = $customer->id;
			$email = md5($customer->email);
			$reset_url = route('reset', compact('user','email','token')); 
			$data = array('email'=>$customer->email, 'name'=> $customer->firstname." ".$customer->lastname );
			Mail::send('emails.password', ['reset_url' => $reset_url, 'username'=> $customer->username], function($message) use ($data)
			{
				$message->from('r88bikecafe@gmail.com', 'Route 88 Bike Café');
				$message->to($data['email'], $data['name'])->subject('Route 88 Bike Café - Request to Reset Password');
			});
		} 
		return redirect("/reset/success/");
	}

	public function resetPassword($user, $email, $token){
		$now = time();
		if(($now-$token) < 86400){ //check if link has been generated within the last 24 hours
			$customer = Customer::find($user);
			if(count($customer)>0){
				return redirect('/reset/password')
				->with('uid',$user)
				->with('token',$token);
			}
		} 
		else {
			return redirect('/reset/expired');
		}
	}

	public function password(){
		if(session('uid') && session('token')){
			$uid = session('uid');
			$token = session('token');
			return view('users.password',compact('uid','token'));
		} else {
			return redirect('/reset/error');
		}
	}

	public function postPassword(){
		$uid = Input::get('uid');
		$password = Input::get('password');
		$customer = Customer::find($uid);
		if($customer->id){
			try {
			$customer->password = md5($password);
            $customer->save();
            return redirect("/password/success");
	        } catch (QueryException $e) {
	        	return redirect("/password/error");
	        }
		}
		return redirect("/password/error");
		
	}

	public function updatePassword(){
		$uid = Input::get('customer_id');
		$password = Input::get('new_password');
		$customer = Customer::find($uid);
		if($customer->id){
			try {
			$customer->password = md5($password);
            $customer->save();
            	echo "success";
	        } catch (QueryException $e) {
	        	echo "error";
	        }
		} else {
			echo "Customer not found";
		}
	}

	public function verifyPassword($id){
		$password = Input::get('current_password');
		$password = md5(trim($password));
		$customer = Customer::where(['id'=>$id,'password'=>$password])->get();
		if(count($customer)>0){
			echo "true";
		} else {
			echo "false";
		}
	}

	public function validateUsername(){
		$username = Input::get('username');
		$customer = Customer::where(['username'=>$username])->get();
		if(count($customer) > 0){
			echo "false";
		} else {
			echo "true";
		}
	}

	public function validateEmail(){
		$email = Input::get('email');
		$customer = Customer::where(['email'=>$email])->get();
		if(count($customer) > 0){
			echo "false";
		} else {
			echo "true";
		}
	}

	public function validateOtherEmail($id){
		$email = Input::get('email');
		$customer = Customer::where(['email'=>$email])->where('id','!=',$id)->get();
		if(count($customer) > 0){
			echo "false";
		} else {
			echo "true";
		}
	}

	public function addAddress(){
		$customerId = Input::get('customer_id');
		$houseNo = Input::get('delivery_house');
		$street = Input::get('delivery_street');
		$area = Input::get('delivery_area');

		$input = Input::all();
        $rules = array(
            'customer_id' => 'required',
            'delivery_house' => 'required',
            'delivery_street' => 'required',
            'delivery_area' => 'required'
        );

        $messages = array(
            'delivery_house.required' => 'Please enter Floor / Building / House No.',
            'delivery_street.required' => 'Please enter Street',
            'delivery_area.required' => 'Please choose Area / District / Barangay'
        );

        $validation = Validator::make($input, $rules,$messages);

        if($validation->passes()){
        	$address = new CustomerAddress;
        	$address->customer_id = $customerId;
        	$address->house_bldg = trim($houseNo);
        	$address->street = trim($street);
        	$address->village_brgy = trim($area);
        	$address->city = "Quezon City";
        	$address->save();

			echo "success";
        } else {
        	echo $validation->messages()->toJson();
        }
		
	}

	public function getAddressList($id){
		$addressList = CustomerAddress::where(['customer_id'=>$id])->get()->toArray();
		echo json_encode($addressList);
	}

	public function getAddressLatest($id){
		$address = CustomerAddress::where(['customer_id'=>$id])->orderBy('created_at','desc')->first()->toArray();
		echo json_encode($address);
	}
}
