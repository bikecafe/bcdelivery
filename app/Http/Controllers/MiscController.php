<?php namespace App\Http\Controllers;

use Session;

class MiscController extends Controller {

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function show(){
		return view('messages.show');
	}

}
