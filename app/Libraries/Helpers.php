<?php namespace App\Libraries;

use App\Setting;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;

class Helpers {

    public static function getSettingValue($name){
        $setting = Setting::where(['name'=>$name])->first();
        return count($setting)>0 ? $setting->value : "";
    }

}