<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerContact extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'customer_contact';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];



}
