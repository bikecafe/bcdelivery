<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'customer_address';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];



}
