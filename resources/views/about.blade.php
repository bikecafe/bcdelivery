@extends('layouts.master')

@section('content')
    <!--About-->    
    <br><br>
    <div class="container">
        <div class="section">
            <div class="row" style="margin-top:80px;">
                <div class="col s12">
                    <div id="header" class="divider"></div>
                    <h5><span class="red-text text-darken-4" style="font-weight:500;">ABOUT ROUTE 88</span></h5>
                    <div id="header" class="divider"></div>
                </div>
            </div>
            <div class="row">
                <div class="col l5 s12 m12">
                    <h6 class="basic-grey" style="line-height:inherit;">Route 88 is a bike shop inspired restaurant / café that offers a wide variety of food from appetizers to pastas, entrées, and beverages such as brewed coffee, milkshakes, and fruit shakes for bikers, families, and young professionals. It surely is a place for leisure and comfort.<br><br>The company also offers for special occassions like birthdays, business meetings, and other similar gatherings.<br><br>
                    <span class="red-text text-darken-4" style="font-size:1.8em; font-weight:500;">OUR MISSION</span>
                    <br><br>It is our aim to serve good food and beverage and satisfy customers with high quality dining experience that would generate sufficient profits and improve the lives of our employees.<br><br>
                    <span class="red-text text-darken-4" style="font-size:1.8em; font-weight:500;">OUR VISION</span>
                    <br><br>By 2020, we will attain a place in the highest echelon of food-chain establishment known for quality products and customer service, compatible and adapted to the living conditions of the greater majority of the people and introduce growth and expansions to reach out to them.</h6>
                </div>
                <div class="col l7 s12 m12">
                <br>
                    <div class="card-panel">
                        <div class="image-container">
                            <img src="/assets/images/img-about.jpg" style="width:100%;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
@endsection