@extends('layouts.master')

@section('content')
    <div id="index-banner" class="parallax-container">
        <div class="section no-pad-bot">
          <div class="container">
          	<h2 class="header center" style="color:#F9FFFF; font-weight:500;">
            <br>For bikers, families, and young professionals</h2>
            <div class="row center"></div>
          	<div class="row center">
          		<a href="/menu" class="btn-large waves-effect waves-light blue darken-3">
                <span style="font-weight:500;"><i class="material-icons left">store</i>ORDER NOW!</span></a>	
            </div>
          </div>
        </div>
    	<div class="parallax"><img src="/assets/images/slide1.jpg" alt="Unsplashed background img 1"></div>
  	</div>
  
  	<div class="container">
        <div class="section">
        <br><br>
            <div class="row">
            <!--row header-->
                <div class="col s12">
                    <div id="header" class="divider"></div>
                    <h5>
                        <span class="red-text text-darken-4" style="font-weight:500;">
                        <i class="material-icons left">restaurant</i>Our Best Sellers
                        </span>
                    </h5>
                    <div id="header" class="divider"></div>
                </div>
            </div>
            <div class="row">
            <!--first card-->
                @foreach($bestSellers as $product)
                <div class="col l4 s12 m4">
                    <div class="card hoverable">
                        <div class="card-image waves-effect waves-block waves-light">
                            <img class="activator" src="{{ Helper::getSettingValue('admin_url') ? Helper::getSettingValue('admin_url') : 'http://bcafe.dev/uploads'}}/{{$product['image']}}">
                        </div>
                        <div class="card-content">
                            <div class="basic-price">₱ {{$product->price}}
                            <span class="activator basic-morevertical">
                            <i class="material-icons right">more_vert</i></span><br>
                            <span class="red-text text-darken-3 product-name">{{$product->name}}</span></div>
                            <br>
                            <div class="divider"></div>
                            <br>
                            <div class="center">
                                <a href="#add-tray" class="btn-large basic-add-tray waves-effect 
                                waves-light blue darken-4 modal-trigger add-to-tray" code="{{$product->id}}" code-name="{{$product->name}}"><i class="material-icons left">add_circle_outline</i>
                                Add to Tray</a>
                            </div>
                        </div>
                        <div class="card-reveal">
                            <span class="card-title"><span class="blue-text text-darken-4 basic-revealtitle">
                            {{$product['name']}}</span>
                            <i class="material-icons right close-icon">close</i></span>
                            <p>{{$product['description']}}</p>
                        </div>  
                    </div>         
                </div>
                @endforeach
            </div>    
            <br>
            <div class="row">
            <!--row header-->
                <div class="col s12">
                    <div id="header" class="divider"></div>
                    <h5>
                        <span class="red-text text-darken-4" style="font-weight:500;">
                            <i class="material-icons left">restaurant</i>Our Entrées
                        </span>
                    </h5>
                    <div id="header" class="divider"></div>
                </div>
            </div>
            <div class="row">
                @foreach($entrees as $product)
                <div class="col l4 s12 m4">
                    <div class="card hoverable">
                        <div class="card-image waves-effect waves-block waves-light">
                            <img class="activator" src="{{ Helper::getSettingValue('admin_url') ? Helper::getSettingValue('admin_url') : 'http://bcafe.dev/uploads'}}/{{$product['image']}}">
                        </div>
                        <div class="card-content">
                            <div class="basic-price">₱ {{$product->price}}
                            <span class="activator basic-morevertical">
                            <i class="material-icons right">more_vert</i></span><br>
                            <span class="red-text text-darken-3 product-name">{{$product->name}}</span></div>
                            <br>
                            <div class="divider"></div>
                            <br>
                            <div class="center">
                                <a href="#add-tray" class="btn-large basic-add-tray waves-effect 
                                waves-light blue darken-4 modal-trigger add-to-tray" code="{{$product->id}}" code-name="{{$product->name}}"><i class="material-icons left">add_circle_outline</i>
                                Add to Tray</a>
                            </div>
                        </div>
                        <div class="card-reveal">
                            <span class="card-title"><span class="blue-text text-darken-4 basic-revealtitle">
                            {{$product['name']}}</span>
                            <i class="material-icons right close-icon">close</i></span>
                            <p>{{$product['description']}}</p>
                        </div>  
                    </div>         
                </div>
                @endforeach
            </div>
            <br>   
            <div class="row center">
                <a href="/menu" class="btn-large waves-effect waves-light red darken-4">
                <span style="font-weight:500;"><i class="material-icons left">restaurant_menu</i>VIEW MENU</span></a>
            </div>
            <br>
        </div>
  	</div>
<!--Intro contact-->  
    <div id="index-banner" class="parallax-container valign-wrapper">
        <div class="section no-pad-bot">
          <div class="container">
            <h4 class="header center" style="color:#F9FFFF; font-weight:500;">
            <i class="material-icons large">question_answer</i></h4>
            <div class="row center">
          		<h2 class="header col s12" style="color:#F9FFFF; font-weight:500;">Got something to say?</h2> 
            </div>
          	<div class="row center">
          		<h5 class="header col s12 light">Feel free to use the form below!</h5>
            </div>	          	
          </div>
        </div>
    	<div class="parallax"><img src="/assets/images/slide2.jpg" alt="Unsplashed background img 1"></div>
  	</div>
<!--Contact-->  
  	<div class="container">
        <div class="section">
        	<br><br>
            <div class="row">
            <!--row header-->
            	<div class="col s12">
                    <div id="header" class="divider"></div>
                    <h5>
                        <span class="red-text text-darken-4" style="font-weight:500;">
                            <i class="material-icons left">phone</i>Contact Us
                        </span>
                    </h5>
                    <div id="header" class="divider"></div>
                </div>
            </div>
            <br>
            <div class="row">
           	<!--contact form-->  
            	<form id="contactForm" action="/contact" method="post" class="col l7 s12 m12">
                	<div class="row">
                        <h6 class="basic-grey col s12" style="line-height:inherit;">
                        Don't forget to send in your complete NAME and EMAIL so we can 
                        get back to you as quickly as possible.</h6>
                    </div>
                	<div class="row">
                        <div class="input-field col s12">
                            <input name="contact_name" id="contact_name" type="text">
                            <label for="contact_name">Your Name (required)</label>
                        </div>
                    </div>
                    <div class="row">    
                        <div class="input-field col s12">
                            <input name="contact_email" id="contact_email" type="email">
                            <label for="contact_email" data-error="invalid!">Your Email (required)</label>
                        </div>
                    </div>
                    <div class="row">    
                        <div class="input-field col s12">
                            <textarea name="contact_message" id="contact_message" class="materialize-textarea"></textarea>
                            <label for="contact_message">Your Message (required)</label>
                        </div>
                    </div>
                    <div class="row center">
                    	<button type="submit" class="btn-large waves-effect waves-light red darken-4">
                		<span style="font-weight:500;"><i class="material-icons left">send</i>SUBMIT</span></button>
                    </div>
                    <br>    
                </form>
            <!--contact details-->  
            	<div class="col l5 s12 m12">
                	<div class="card">
                        <div class="card-image">
                        	<div id="map"></div>
                        </div>
                  
                         <div class="card-action">
                            <ul class="basic-grey">
                            	<li><i class="material-icons left blue-text">location_on</i>
                                88-A Anonas Street, Barangay East Kamias, 1102 Quezon City, Philippines</li><br>
                            	<li><i class="material-icons left blue-text">access_time</i>
                                Mon to Sat:  9:30 A.M - 10:00 P.M</li><br>	
                            	<li><i class="material-icons left blue-text">phone</i>(02) 433 5368</li>
							</ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  	</div>
    <br>
<!--Intro about-->  
    <div id="index-banner" class="parallax-container valign-wrapper">
        <div class="section no-pad-bot">
          <div class="container">
          	<div class="row center">
          		<h4 class="header col s12 light">
                <br>"We at Route 88 aim to satisfy our customers with good food, beverage, & high quality dining experience that would generate sufficient profit and improve the lives of our employees."</h4>
            </div>
            <div class="row center">
          		<a href="/about" class="btn-large waves-effect waves-light blue darken-3">
                <span style="font-weight:500;">READ MORE</a>	
            </div>        	
          </div>
        </div>
    	<div class="parallax"><img src="/assets/images/slide3.jpg" alt="Unsplashed background img 1"></div>
  	</div>

    <!--Add to Tray Modal-->
    <div id="add-tray" class="modal">
        <div class="modal-content">
            <br>
            <div class="row">
                <div class="col l10 offset-l1 col s10 offset-s1 col m10 offset-m1">
                    <h5 id="modal_product_name" class="red-text text-darken-4" style="font-weight:500;"></h5>
                    <div id="header" class="divider"></div>
                </div>
            </div>
            <div class="row">
                <form id="addTrayForm">
                    <br/>
                    <input type="hidden" id="prod_id" name="id" value=""/>
                    <div class="row center">
                        <label for="prod_qty" style="font-size:1.1rem">Order Quantity</label>
                        <input id="prod_qty" name="quantity" type="number" min="1" max="20" value="1" style="display:block;width:100px;margin:0 auto;font-size:16px" />
                    </div>
                    <div class="row center">
                        <div class="col l10 offset-l1 col s10 offset-s1 col m10 offset-m1">
                            <span style="text-align:center; color:#DE0A16;font-size:12px"><b>Note:</b> Product is subject to availability upon ordering. <br/>For bulk orders, please call us at (02) 433 5368.</span>
                        </div>
                    </div>
                    <div class="row center">
                        <button type="submit" class="btn waves-effect waves-light red darken-4">
                                    ADD TO TRAY</button>
                        <a href="#!" id="cancel_tray" class="btn waves-effect waves-light red darken-4 modal-close">
                                    CANCEL</a>
                    </div>
                </form>
            </div>
        </div>
    </div>    
@endsection

@section('script')
<script src="/assets/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="/assets/js/jquery.form.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.basic-add-tray').click(function(){
            var product_name = $(this).parents('.card-content').find('.product-name').html();
            $("#modal_product_name").html(product_name.toUpperCase());
            $("#prod_id").val($(this).attr('code'));
            $("#add-tray").openModal();            
        });

        $("#addTrayForm").submit(function(e){
            e.preventDefault();
            $('#addTrayForm').ajaxSubmit({
              type: "POST",
              url: "tray/add",
              dataType: 'json',
              success: function(data){
                    $("#cancel_tray").trigger("click");
                    document.getElementById("addTrayForm").reset();
                    //if add to tray successful
                    if(data.status == 1){
                        console.log(data.added);
                        //show success message for 5 seconds
                        $('#flash_message').html('Order successfully added!');
                        $('#flash_message').show('fast');
                        setTimeout(function() {
                            $('#flash_message').fadeOut('slow');
                        }, 2000);
                    }
              },
              error: function(xHr, textStatus, errorThrown) {
                 console.log(xHr.responseText);
                 console.log(errorThrown);
              }
            });
        });

        $("#contactForm").validate({
            rules: {
                contact_name : {
                    required: true
                },
                contact_email : {
                    required: true,
                    email: true
                },
                contact_message: {
                    required: true
                }
            },
            messages: {
                contact_name : {
                    required: "Please enter your name"
                },
                contact_email : {
                    required: "Please enter your e-mail",
                    email: "Please provide a valid e-mail"
                },
                contact_message: {
                    required: "Please enter your message"
                }
            },
            errorElement : 'span',
            errorPlacement: function(error, element) {
                error.css({ 'color': '#B71C1C', 'font-size': '0.750em', 'position':'absolute', 'top':'50px' });   
                error.appendTo( element.parent() );
            }
        });
    });

    function initialize() {
      var myLatLng = {lat: 14.633555, lng: 121.059295};

      var mapProp = {
        center:new google.maps.LatLng(14.633555, 121.059295),
        zoom:18,
        mapTypeId:google.maps.MapTypeId.ROADMAP
      };
      var map=new google.maps.Map(document.getElementById("map"),mapProp);
      var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Route 88 Bike Café'
      });
      var infowindow = new google.maps.InfoWindow({
        content: 'Route 88 Bike Café'
      });

      marker.addListener('click', function() {
        infowindow.open(map, marker);
      });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
        
</script>
@endsection