@extends('layouts.master')

@section('content')
    <br><br>
    <div class="container">
        <div class="row" style="margin-top:80px";>
            <div class="col s12">
                <div id="header" class="divider"></div>
                <h5><span class="red-text text-darken-4" style="font-weight:500;">FOOD TRAY</span></h5>
                <div id="header" class="divider"></div>
            </div>
        </div>
        @if(isset($orders) && !empty($orders))
        <div class="row right">
            <div class="col s12">
                <button type="submit" id="clear_tray" class="btn basic-add-tray waves-effect waves-light red darken-4">
                CLEAR TRAY</button>
            </div>
        </div>
        <div class="row">                   
            <div class="col s12 m12 l12">
                <table id="order_tray" class="striped centered responsive-table">
                    <thead>
                    <tr>
                    <th data-field="item">Item</th>
                    <th data-field="price">Price</th>
                    <th data-field="quantity">Quantity</th>
                    <th data-field="cost">Cost</th>
                    <th data-field="edit-remove">Remove</th>
                    </tr>
                    </thead>

                <tbody>
                 @if(isset($orders) && !empty($orders))
                  @foreach($orders as $order)
                  <tr>
                    <td>{{ $order['name'] }}</td>
                    <td>₱ {{ number_format($order['price'],2) }}</td>
                    <td>
                    <form>
                        <input class="row-quantity" type="number" data-id="{{ $order['id'] }}" min="1" max="20" value="{{ $order['quantity'] }}">
                    </form>
                    </td>
                    <td>₱ <span class="row-cost">{{ number_format($order['quantity'] * $order['price'],2) }}</span></td>
                    <td><a class="btn-flat red-text text-darken-4 clear-row" href="#!" data-id="{{ $order['id'] }}">
                    <i class="large material-icons">clear</i></a></td>
                  </tr>
                  @endforeach
                @endif
                </tbody>
              </table>
            </div>
        </div>
        <br>
        <div class="row right">
            <div class="col s12"><h6 id="total_cost" class="red-text text-darken-4" 
            style="font-weight: 600; font-size:1.10em;">Total Cost: ₱ <span>{{ number_format(Cart::getTotal(),2) }}</span></h6></div>
        </div>
        @if(empty($customer))
        <div class="row">                   
            <div class="col s12 m12 l12" style="color:#de0a16">
                Please <a href="/login">log in</a> to checkout your orders.
            </div>
        </div>
        @endif
        @endif

        @if(isset($orders) && empty($orders))
        <div class="row">                   
            <div class="col s12 m12 l12">
                No orders in tray
            </div>
        </div>
        @endif
        <br>
        


        @if(isset($customer) && !empty($customer) && isset($orders) && !empty($orders))
        <div class="row">
            <div class="col s12">
                <div id="register" class="divider hide-on-med-and-up"></div>
                <h5 class="blue-text text-darken-4">Customer Details</h5>   
                <div id="register" class="divider"></div>  
            </div>    
        </div>
        
        <div class="row">
            <div class="col s4 m3 l3 add-c-profile">
                <table>
                <tbody>
                    <tr><td class="grey lighten-4 red-text text-darken-4">Name</td></tr>
                    <tr><td class="grey lighten-4 red-text text-darken-4">Address</td></tr>
                    <tr><td class="grey lighten-4 red-text text-darken-4">Contact Number</td></tr>
                    <tr><td class="grey lighten-4 red-text text-darken-4">E-mail Address</td></tr>
                </tbody>
                </table>
            </div>
            <div class="col s8 m9 l9 add-c-profile">
                 <input type="hidden" name="user_address" value="{{$customer->getPrimaryAddress()}}"/>
                 <table>
                 <tbody>
                    <tr><td>{{ $customer->firstname . " " . $customer->lastname }}</td></tr>
                    <tr><td>{{ $customer->getPrimaryAddress() ? $customer->getPrimaryAddress()->house_bldg . " " . $customer->getPrimaryAddress()->street . " " . $customer->getPrimaryAddress()->village_brgy . " " . $customer->getPrimaryAddress()->city : 'No Registered Address'}}</td></tr>
                    <tr><td>{{ $customer->getLatestContact()->contact_number }}</td></tr>
                    <tr><td>{{ $customer->email }}</td></tr>
                 </tbody>
                 </table>
            <br>    
            </div>
        </div>
        <div class="row">
            <div class="col s12 m12 l12" style="color:#de0a16">{{ $customer->is_email_verified == 0 ? 'NOTICE: Your e-mail address needs to be verified before you can checkout your order tray.' : ''}}</div>                   
            <div class="col s12 m12 l12" style="color:#de0a16">
                Please ensure that your information is correct or we cannot guarantee your order. Update your profile <a href="/account/">here</a>.
            </div>
        </div>
        <form method="POST" enctype="multipart/form-data" class="col s12" id="checkoutForm">
            <div class="row">
                <div class="col s12">
                    <div id="register" class="divider hide-on-med-and-up"></div>
                    <h5 class="blue-text text-darken-4">Order Comments</h5> 
                    <div id="register" class="divider"></div>  
                </div>    
            </div>
        
            <input id="total-order" type="hidden" name="total_cost" value="{{ Cart::getTotal() }}" />
            <div class="row">  
                <h6 class="basic-grey col s12" style="line-height:inherit;">
                    (Optional) Send in your additional instructions or special requests regarding your order here.</h6>
            </div>        
            <div class="row">
                
                    <div class="input-field col s12">
                        <textarea id="" class="materialize-textarea" name="comments"></textarea>
                        <label for="" class="">Your Comments</label>
                    </div>
            </div>

            <div class="row">
                <div class="col s12">
                    <div id="register" class="divider hide-on-med-and-up"></div>
                    <h5 class="blue-text text-darken-4">Order Preferences</h5> 
                    <div id="register" class="divider"></div>  
                </div>    
            </div>
            <div class="row">  
                <h6 class="basic-grey col s12" style="line-height:inherit;">
                    How would you like to acquire your orders?</h6>
            </div>        
            <div class="row">
                <div class="col s12">
                    <p>
                        <input name="acquisition" type="radio" id="radio-pickup" value="1"/>
                        <label for="radio-pickup">Pickup</label>
                    </p>
                    <p id="acquisition_last">
                        <input name="acquisition" type="radio" id="radio-delivery" value="2"/>
                        <label for="radio-delivery">Delivery</label>
                    </p>
                </div>
            </div>
            <div class="row require-address hidden">
                <div class="col s12">
                    <p style="color: #B71C1C !important;">**You do not have a registered address. Please edit your profile and register an address first.</p>
                </div>
            </div>  
            <div class="row">  
                <h6 class="basic-grey col s12" style="line-height:inherit;">
                    Prepare change for ₱ <input name="change_for" type="text" style="display:inline;width:80px" onkeypress="return isNumberKey(this,event);" onpaste="return false;"/> </h6> 
            </div>       
    
            <div class="row center hide-on-small-only">
                <div class="col s12">
                    <a href="/menu" class="btn-large basic-add-tray waves-effect waves-light blue darken-4">BACK TO MENU</a>
                    
                    <button type="{{ $customer->is_email_verified == 0 ? 'button' : 'submit' }}" class="btn-large waves-effect 
                    waves-light {{ $customer->is_email_verified == 0 ? 'grey' : 'blue' }} darken-4" {{ $customer->is_email_verified == 0 ? 'disabled' : '' }}>PLACE ORDER</button>
                </div>
            </div>
        
    
            <div class="row center hide-on-med-and-up">
                <div class="col s12">
                    <div class="row">
                        <a href="/menu" class="col s12 btn-large basic-add-tray waves-effect 
                        waves-light blue darken-4">BACK TO MENU</a>
                    </div>
                    <div class="row">
                        <button type="{{ $customer->is_email_verified == 0 ? 'button' : 'submit' }}" class="col s12 btn-large waves-effect 
                        waves-light {{ $customer->is_email_verified == 0 ? 'grey' : 'blue' }} darken-4" {{ $customer->is_email_verified == 0 ? 'disabled' : '' }}>PLACE ORDER</button>
                    </div>
                </div>
            </div>
        </form>
        @endif
    </div>
<!--Delivery Modal-->
<div id="modaldelivery" class="modal basic-modal">
    <div class="modal-content">
        <form action="#">
            <p>
                <input name="tray-delivery" type="radio" id="radio-pickup"/>
                <label for="radio-pickup">Pickup</label>
            </p>
            <p>
                <input name="tray-delivery" type="radio" id="radio-delivery"/>
                <label for="radio-delivery">Delivery</label>
            </p>
        </form>
    </div>
    <div class="modal-footer basic-modalfooter">
      <a href="#!" class="modal-action modal-close waves-effect waves-light blue darken-4 btn">CONFIRM</a>
    </div>
</div>     
<br>        
@endsection

@section('script')
<script src="/assets/js/jquery.validate.min.js" type="text/javascript"></script>
<!-- Sweet Alert -->
<link rel="stylesheet" href="/assets/css/sweet-alert/sweetalert2.css" />
<!-- Sweet Alert -->
<script src="/assets/js/sweet-alert/sweetalert2.min.js"></script>
<script>
    $(document).ready( function () {
        $('.modal-trigger').leanModal();
        $("#clear_tray").click(function(event){
            event.preventDefault();
            $.get( "/tray/clear", function( data ) {
                console.log('result:'+data);
                location.reload();
            });
        });
        $('#order_tray td').each(function(){
            $(this).contents().wrapAll('<div class="slide" />');
        });
        $("#order_tray .clear-row").click(function(event){
            event.preventDefault();
            id = $(this).attr('data-id');
            row = this;
            $.get("/tray/"+id+"/clear", function(data){
                console.log(data);
                if(data == "order removed"){
                    $(row).parents('tr').find('.slide').slideUp(500, function(){
                        $(row).parents('tr')[0].remove();
                        updateTotalCost();
                        //update tray count
                        $.get( "/tray/count", function( data ) {
                            if(parseInt(data) > 0){
                                $("#tray_count").html(data);
                                $("#mobile_cart_total").html("("+data+")");
                            } else {
                                $("#tray_count").html("");
                                $("#mobile_cart_total").html("");
                            }
                            console.log('tray:'+data);
                        });
                        if($("#order_tray tbody tr").length == 0){
                            location.reload(true);
                        }
                    });
                }
            });

        });  
        $("#order_tray .row-quantity").change(function(){
            id = $(this).attr('data-id');
            qty = $(this).val();
            row = this;
            $.get("/tray/"+id+"/quantity/"+qty,function(data){
                console.log(data);
                //update row cost
                $(row).parents('tr').find('.row-cost').html(parseFloat(data).toFixed(2));
                updateTotalCost();
            });
        });
        function updateTotalCost(){
            $.get("/tray/cost",function(data){
                $("#total_cost").find("span").html(parseFloat(data).toFixed(2));
                $("input[name=total_cost]").val(parseFloat(data));
            })
        }  

        //validate registered address if "delivery" method was chosen
        var form_error = false;
        $("input[type='radio']").click(function(){
            var radioValue = $("input[name='acquisition']:checked").val();
            if(radioValue == 2){
                var user_address = $("[name=user_address]").val();
                if(user_address.trim() == ""){
                    $(".require-address").show('fast');
                    form_error == true;
                }
            } else {
                $(".require-address").hide('fast');
                form_error == false;
            }
        });

        $.validator.addMethod('greaterOrEqualTo', function(value, element, param) {
            return parseFloat(value) >= parseFloat($(param).val());
        }, "Entered value is lower than your total order cost");
        
        $("#checkoutForm").validate({
            rules: {
                acquisition: {
                    required: true
                },
                change_for : {
                    required: true,
                    greaterOrEqualTo: '#total-order'
                }
            },
            messages: {
                acquisition: {
                    required: "Please choose your preference"
                },
                change_for: {
                    required: "Please enter your prepared cash on hand for payment"
                }
            },
            submitHandler: function(form){
                if(!$(".require-address").is(":visible")){
                    var form = $(form);
                    swal({
                            title: "Are your orders final?",
                            text: "",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#0D47A1",
                            confirmButtonText: "Yes, place my orders now!",
                            cancelButtonText: "Cancel",
                            closeOnConfirm: true,   closeOnCancel: true },
                        function(isConfirm){
                            if (isConfirm) {
                                form.unbind('submit').submit();
                            }
                        }
                    );
                }
            },
            errorElement : 'span',
            errorPlacement: function(error, element) {
                error.css({ 'color': '#B71C1C', 'font-size': '0.750em', 'position':'relative', 'display':'block' });   
                if (element.attr("name") == "acquisition"){
                    error.insertAfter("#acquisition_last");
                } else {
                    error.appendTo( element.parent() );
                }
            }
        });

        
    });

    function isNumberKey(txt, evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 46) {
            //Check if the text already contains the . character
            if (txt.value.indexOf('.') === -1) {
                return true;
            } else {
                return false;
            }
        } else {
            if (charCode > 31
                 && (charCode < 48 || charCode > 57))
                return false;
        }
        return true;
    }
</script>
@endsection