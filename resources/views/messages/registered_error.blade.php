@extends('layouts.master')

@section('content')
    <!--Sign up-->    
    <br><br>
    <div class="container">
        <div class="section">
            <div class="row" style="margin-top:80px;">
                <div class="basic-link col l8 offset-l2 s12 m8 offset-m2">
                    <h6></h6>
                </div>
            </div> 
            <div class="row">
                <div class="col l8 offset-l2 s12 m8 offset-m2">
                    <div class="card hoverable">
                    <br><br>
                    <!--Log in header-->
                        <div class="row">
                            <div class="col l10 offset-l1 s10 offset-s1 m10 offset-m1" style="min-height:150px">
                                <h5><span class="red-text text-darken-4" style="font-weight:500;">
                                Registration Failed!</span></h5>
                                <div id="header" class="divider">
                                </div>
                                <p>An unexpected error occurred. Please try again later or contact us regarding this problem.</p>
                            </div>
                        </div>
                        
                    </div>  
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script>
    
</script>
@endsection