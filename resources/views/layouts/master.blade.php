<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title>Route 88 Bike Café</title>
    
    <!--<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">-->
    <link href="/assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="/assets/css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link rel="icon" type="image/x-icon" href="assets/images/logo.ico" />
</head>

<body>
    <nav>
          <div class="nav-wrapper container">
            <a href="/" class="brand-logo"><img class="responsive-img" src="/assets/images/logo.png"></a>
            <a href="#!" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
            @if(session('curr_id') && (isset($customer) && !empty($customer)))
            <div class="welcome-user">
                Welcome, {{ $customer->firstname }}!
            </div>
            @endif
            <ul class="right hide-on-med-and-down">
                @if(session('curr_id'))
                <li><a class="{{ Request::is('account') ? 'active' : '' }}" href="/account">My Account</a></li>
                @endif
                <li><a class="{{ Request::is('/') ? 'active' : '' }}" href="/">Home</a></li>
                <li><a class="{{ Request::is('menu*') ? 'active' : '' }}" href="/menu">Menu</a></li>
                @if(!session('curr_id'))
                <li><a class="{{ Request::is('login') ? 'active' : '' }}" href="/login">Log In</a></li>
                @endif
                <li><a class="{{ Request::is('tray*') ? 'active' : '' }}" href="/tray"><span id="tray_count" style="display:block;font-size:12px;text-align:center;position:absolute;top:-20px;width:32px;">{{ count(Cart::getContent()) > 0 ? count(Cart::getContent()) : ''}}</span><i class="material-icons">shopping_cart</i></a></li>
                @if(session('curr_id'))
                <li><a href="/logout"><i class="material-icons">exit_to_app</i></a></li>
                @endif
            </ul>
            <div class="side-nav" id="nav-mobile">
                @if(session('curr_id') && (isset($customer) && !empty($customer)))
                <div class="welcome-user-mobile">
                    <i class="material-icons">account_circle</i> Welcome, {{ $customer->firstname }}!
                </div>
                @endif
                <ul>
                    <li class="{{Request::is('/') ? 'active' : ''}}"><a class="{{Request::is('/') ? 'active' : ''}}" href="/">Home</a></li>
                    @if(session('curr_id'))
                    <li class="{{Request::is('account*') ? 'active' : ''}}"><a class="{{Request::is('account*') ? 'active' : ''}}" href="/account">My Account</a></li>
                    @endif
                    <li class="{{Request::is('menu*') ? 'active' : ''}}"><a class="{{Request::is('menu*') ? 'active' : ''}}" href="/menu">Menu</a></li>
                    @if(!session('curr_id'))
                    <li class="{{Request::is('login') ? 'active' : ''}}"><a class="{{Request::is('login') ? 'active' : ''}}" href="/login">Log In</a></li>
                    @endif
                    <li class="{{Request::is('tray*') ? 'active' : ''}}"><a class="{{Request::is('tray*') ? 'active' : ''}}" href="/tray">Order Tray <span id="mobile_cart_total">{{ count(Cart::getContent()) > 0 ? '('.count(Cart::getContent()).')' : ''}}</span></a></li>
                    @if(session('curr_id'))
                    <li><a href="/logout">Log Out</a></li>
                    @endif
                </ul>
            </div>
          </div>
    </nav>
    @yield('content')
<!--Footer-->
    <div id="flash_message" style="display:none;background-color: #fffb96;width: 100%;position: fixed;top: 0;z-index: 100;border: 1px solid #c9bf0c;padding: 10px;color: #000913;font-family: 'Roboto', sans-serif;">{{ Session::has('flash_message') ? Session::get('flash_message') : ''}}</div>
    <footer class="page-footer">
        <div class="container">
            <div class="row">
            <!--Links-->
                <!-- <div class="col l3 s12 m3">
                    <h6 class="red-text text-darken-4" style="font-size:1.4em; font-weight:600;">Menu</h6>
                    <ul class="light">
                      <li><a href="#!">Omelette</a></li>
                      <li><a href="#!">All Day Breakfast</a></li>
                      <li><a href="#!">Tempting Starters</a></li>
                      <li><a href="#!">Entrée</a></li>
                      <li><a href="#!">Sandwich Corner</a></li>
                      <li><a href="#!">Squad Meals</a></li>
                      <li><a href="#!">Beverages</a></li>
                    </ul>
                    <div class="divider hide-on-med-and-up"></div>
                    <br>
                </div> -->
            <!--Links-->
                <div class="col l3 s12 m3">
                    <h6 class="red-text text-darken-4" style="font-size:1.4em; font-weight:600;">Quick Links</h6>
                    <ul class="light">
                      <li><a href="/">Home</a></li>
                      <li><a href="/menu">Menu</a></li>
                      <li><a href="/about">About Us</a></li>
                      @if(session('curr_id'))
                      <li><a href="/account">My Account</a></li>
                      @endif
                    </ul>
                    <div class="divider hide-on-med-and-up"></div>
                    <br>
                </div>    
            <!--About-->
                <div class="col l9 s12 m6">
                    <h6 class="red-text text-darken-4" style="font-size:1.4em; font-weight:600;">Route 88 Bike Café</h6>
                    <h6 class="white-text light" style="line-height:inherit;">A bike-inspired café / restaurant that offers customers delicious meals at a friendly cost.</h6> 
                    <br>
                    <div class="left-align">
                        <a href="https://www.facebook.com/route88bikecafe" class="btn waves-effect waves-light blue darken-4">
                        LIKE US ON FACEBOOK</a>
                    </div>
                    <br>    
                </div>          
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                <p style="font-weight:300;">© 2016 <a href="/">Route 88 Bike Café</a>. All Rights Reserved.</p>
            </div>
        </div>
    </footer> 

</body>
    <!--<script type="text/javascript"src="https://code.jquery.com/jquery-2.1.1.min.js"></script>-->
    <script src="/assets/js/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/assets/js/materialize.js"></script>
    <script src="/assets/js/init.js"></script>
    <script src="http://maps.googleapis.com/maps/api/js"></script>
    
    <script type="text/javascript">
    
        $(document).ready( function () {
            $('select').material_select();
            $(".button-collapse").sideNav();
            
            //var t=setInterval(updateTray,3000);

            function updateTray(){
                $.get( "/tray/count", function( data ) {
                    if(parseInt(data) > 0){
                        $("#tray_count").html(data);
                    } else {
                        $("#tray_count").html("");
                    }
                    console.log('tray:'+data);
                });

            }
        });
    
    </script>

    @yield('script', '')
</html>