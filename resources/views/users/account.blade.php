@extends('layouts.master')

@section('content')
    <br><br>
    
    <!--Content-->
    <div class="container">
        <div class="section">
            <div class="row" style="margin-top:80px";>
                <div class="col s12">
                    <div id="header" class="divider"></div>
                    <h5><span class="red-text text-darken-4" style="font-weight:500;">Welcome, {{ strtoupper($customer->firstname)}}!</span></h5>
                    <div id="header" class="divider"></div>
                </div>
            </div>
            
            <div class="row">
                <div class="col s12 l4 offset-l4">
                <ul class="tabs">
                    <li class="col s6 tab" style="font-weight:500;"><a href="#my-profile">MY PROFILE</a></li>
                    <li class="col s6 tab" style="font-weight:500;"><a href="#my-order">ORDER HISTORY</a></li>
                </ul>
                </div>
           </div>
           
          <div id="my-profile">
            <div class="row">                   
                <div class="col s4 m3 l3 add-c-profile">
                    <table>
                    <tbody>
                        <tr><td class="grey lighten-4 red-text text-darken-4">Name</td></tr>
                        <tr><td class="grey lighten-4 red-text text-darken-4">Username</td></tr>
                        <tr><td class="grey lighten-4 red-text text-darken-4">Primary Address</td></tr>
                        <tr><td class="grey lighten-4 red-text text-darken-4">Contact Number</td></tr>
                        <tr><td class="grey lighten-4 red-text text-darken-4">Email Address</td></tr>
                    </tbody>
                    </table>
                </div>
                <div class="col s8 m9 l9 add-c-profile">
                     <table>
                     <tbody>
                        <tr><td>{{ $customer->firstname . ' ' . $customer->lastname}}</td></tr>
                        <tr><td>{{ $customer->username }}</td></tr>
                        <tr><td>{{ $customer->getPrimaryAddress() ? $customer->getPrimaryAddress()->house_bldg . " " . $customer->getPrimaryAddress()->street . " " . $customer->getPrimaryAddress()->village_brgy . " " . $customer->getPrimaryAddress()->city : 'No Registered Address'}}</td></tr>
                        <tr><td>{{ $customer->getLatestContact()->contact_number }}</td></tr>
                        <tr><td>{{ $customer->email }}</td></tr>
                     </tbody>
                     </table>
                <br><br>    
                </div>
                <div style="display: block;width: 100%!important;text-align: center;margin-bottom: 30px;color: #B71C1C;"><?php echo $customer->is_email_verified == 0 ? 'NOTICE: Your e-mail address needs to be verified before you can order online. Click <a href="/verification">here</a> to send another e-mail if you didn\'t receive any.' : '' ?></div>
                <div class="row center">
                        <a class="btn-editprofile btn waves-effect waves-light blue darken-4" target="1">
                        EDIT PROFILE</a>
                        <a href="#modal-pw" class="btn waves-effect waves-light blue darken-4 modal-trigger">
                        CHANGE PASSWORD</a>
                </div>
                <br>
                <!--Change Password-->
                <div id="modal-pw" class="modal">
                    <div class="modal-content">
                    <br>
                        <div class="row">
                            <div class="col l10 offset-l1 col s10 offset-s1 col m10 offset-m1">
                                <h5 class="red-text text-darken-4" style="font-weight:500;">PASSWORD CHANGE</h5>
                                <div id="header" class="divider"></div>
                            </div>
                        </div>
                        <div class="row">
                            <form id="changePasswordForm" class="col s12">
                                <input type="hidden" id="cust_id" name="customer_id" value="{{ $customer->id }}"/>
                                <div class="row">
                                    <div class="input-field col l10 offset-l1 col s10 offset-s1 col m10 offset-m1">
                                    <input id="current_password" type="password" name="current_password">
                                    <label for="current_password" class="">Current Password</label>
                                    </div>
                                    <div class="col l10 offset-l1 col s10 offset-s1 col m10 offset-m1">
                                        <span class="basic-link right">
                                        <a href="/reset">Forgot Password?</a></span>
                                    </div>
                                </div>            
                                <div class="row">
                                    <div class="input-field col l5 offset-l1 col s10 offset-s1 col m10 offset-m1">
                                    <input id="new_password" type="password" name="new_password">
                                    <label for="new_password" class="">New Password</label>
                                    </div>
                                    <div class="input-field col l5 col s10 offset-s1 col m10 offset-m1">
                                    <input id="confirm_password" type="password" name="confirm_password">
                                    <label for="confirm_password" class="">Verify Password</label>
                                    </div>
                                </div>
                                <br>
                                <div class="row center">
                                    <button type="submit" class="btn waves-effect waves-light red darken-4">
                                    SAVE CHANGES</button>
                                    <a href="#!" id="cancel_password" class="btn waves-effect waves-light red darken-4 modal-close">
                                    CANCEL</a>
                                </div>
                            </form>        
                        </div>                
                    </div>
                </div>
                <!--Edit Profile-->
                <div id="profile1" class="{{isset($edit)?'':'hidden'}} targetMenu">
                    <div class="row">
                        <div class="col s12">
                        <div id="register" class="divider hide-on-med-and-up"></div>
                        <h5 class="blue-text text-darken-4">
                        <i class="material-icons left">edit</i>Personal Details</h5>
                        <div id="register" class="divider"></div>
                        </div>
                    </div>
                    <div class="row">
                        <form id="saveProfile" method="POST" enctype="multipart/form-data" class="col s12">
                            <div class="row">
                                <div class="input-field col l6 s12 m6">
                                <input id="register_firstname" name="firstname" type="text" value="{{ Input::old('firstname', isset($customer)?$customer->firstname:'')  }}">
                                <label for="register_firstname" class="">First Name</label>
                                </div>
                                <div class="input-field col l6 s12 m6">
                                <input id="register_lastname" name="lastname" type="text" value="{{ Input::old('lastname', isset($customer)?$customer->lastname:'') }}">
                                <label for="register_lastname" class="">Last Name</label>
                                </div>
                            </div>                           
                            <div class="row">    
                                <div class="input-field col s12">
                                    <input id="register_email" type="email" name="email" value="{{ $customer->email }}">
                                    <label for="register_email" data-error="invalid!" class="">Email</label>
                                </div>
                                <div class="col s12">
                                    <span class="right" style="color:#039BE5;">
                                    Updating e-mail address requires validation</span>
                                </div>
                            </div>
                            <div class="row">    
                                <div class="input-field col s12">
                                    <input id="register_number" type="tel" name="contact" value="{{ $customer->getLatestContact()->contact_number }}">
                                    <label for="register_number" class="">Contact Number</label>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col s12">
                                    <div id="register" class="divider hide-on-med-and-up"></div>
                                    <h5 class="blue-text text-darken-4">
                                    <i class="material-icons left">edit</i>Delivery Address</h5>    
                                    <div id="register" class="divider"></div>  
                                </div>    
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <select id="primary-address" name="primary_address">
                                        <option value="" disabled>Select Primary Delivery Address</option>
                                        @foreach($customer->addresses as $address)
                                        <option value="{{ $address->id }}" {{ $address->is_primary == 1 ? 'selected' : ''}}>{{ $address->house_bldg . " " . $address->street . " " . $address->village_brgy . " " . $address->city }}</option>
                                        @endforeach
                                        
                                    </select>
                                    <label>Primary Delivery Address</label> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12">
                                    <a href="#modal-address" class="btn waves-effect waves-light blue darken-4 modal-trigger">
                                    <i class="large material-icons left">add</i>ADD ADDRESS</a> 
                                </div>
                            </div>
                            <br><br>
                            <div class="row center">
                            <!--For mobile-->
                                <div class="col s12 hide-on-med-and-up">
                                    <div class="row">
                                    <button type="submit" id="saveProfile-buttonm" value="submit" class="col s12 btn-large 
                                    waves-effect waves-light red darken-4" style="height:64px; line-height:66px;">
                                    <span style="font-weight:500;">SAVE</span></button>
                                    </div>
                                    <div class="row">
                                    <button type="button" href="#!" class="col s12 btn-large waves-effect waves-light 
                                    red darken-4 cancel-edit" target="1" style="height:64px; line-height:66px;">
                                    <span style="font-weight:500;">CANCEL</span></button>
                                    </div>
                                </div>
                            <!--For large screen-->
                                <div class="col s12 hide-on-small-only">
                                    <button type="submit" id="saveProfile-button" value="submit" class="btn-large waves-effect 
                                    waves-light red darken-4"><span style="font-weight:500;">SAVE</span></button>
                                    <button type="button" href="#!" class="btn-large waves-effect waves-light red darken-4 cancel-edit" target="1">
                                    <span style="font-weight:500;">CANCEL</span></button>
                                </div>   
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="my-order">
            <div class="row">                   
                <div class="col s12 m12 l12">
                    <ul class="collapsible" data-collapsible="accordion">
                    <!--Li Per order-->
                    @foreach($customer->ordersDescending() as $order)
                        <li>
                          <div class="collapsible-header">
                            <i class="large material-icons">chevron_right</i><span class="order-row-status" style="{{ strtoupper($order->status->name) == 'DELIVERED' ? 'color:green' : ''}}">[{{ strtoupper($order->status->name) }}] </span><strong>{{ date("F j, Y h:i a",strtotime($order->created_at)) }}</strong>
                            
                            <span class="order-row-total" style="float:right"><b>GRAND TOTAL:</b> ₱ {{ number_format($order->total_price,2) }}</span>
                          </div>
                          <div class="collapsible-body">
                            
                            <table class="highlight centered responsive-table">
                                <thead>
                                <tr>
                                <th data-field="item">Item</th>
                                <th data-field="cost">Unit Cost</th>
                                <th data-field="quantity">Quantity</th>
                                <th data-fied="status">Subtotal</th>
                                </tr>
                                </thead>
                                
                                <tbody>
                                    @foreach($order->orderlines as $orderline)
                                    <tr>
                                        <td>{{ $orderline->product->name }}</td>
                                        <td>₱ {{ $orderline->product->price }}</td>
                                        <td>x{{ $orderline->quantity }}</td>
                                        <td>₱ {{ number_format($orderline->quantity * $orderline->price,2)}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                          </div>
                        </li>
                    @endforeach
                    </ul>
                    
                </div>
            </div>      
        </div>
                
    </div>
 </div>   
<br>
<!--Add Address-->
<div id="modal-address" class="modal">
    <div class="modal-content">
    <br>
        <div class="row">
            <div class="col l10 offset-l1 col s10 offset-s1 col m10 offset-m1">
            <h5 class="red-text text-darken-4" style="font-weight:500;">ADD ADDRESS</h5>
            <div id="header" class="divider"></div>
            </div>
        </div>
        <div class="row">
            <form id="addressForm" action="/account/address" method="POST" enctype="multipart/form-data" class="col s12">
                <input type="hidden" id="cust_id" name="customer_id" value="{{ $customer->id }}"/>
                <div class="row">
                    <div class="input-field col l10 offset-l1 col s10 offset-s1 col m10 offset-m1">
                        <input id="add_floor" type="text" name="delivery_house">
                        <label for="add_floor">Floor / Building / House No.</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col l10 offset-l1 col s10 offset-s1 col m10 offset-m1">
                        <input id="add_street" type="text" name="delivery_street">
                        <label for="add_street">Street</label>
                    </div>
                </div>    
                <div class="row">
                    <div class="input-field col l10 offset-l1 col s10 offset-s1 col m10 offset-m1">
                        <select id="delivery-area" name="delivery_area"> 
                          <option value="default" disabled selected>Area / District / Barangay *</option>
                          @foreach($delivery_area as $area)
                          <option value="{{ $area }}">{{$area}}</option>
                          @endforeach
                        </select>
                        <label for="add_area">Area / District / Barangay</label>
                    </div>
                </div>
                
                <br>
                <div class="row center">
                    <input id="save_address" type="submit" class="btn waves-effect waves-light red darken-4" value="SAVE CHANGES"/>
                    <a href="#!" id="cancel_address" class="btn modal-close waves-effect waves-light red darken-4">CANCEL</a>
                </div>
            </form>        
        </div>                
    </div>
</div>

@endsection

@section('script')
<script src="/assets/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="/assets/js/jquery.form.min.js" type="text/javascript"></script>
<script>

    $(document).ready( function () {
        var customer_id = $('#cust_id').val();
        $(".button-collapse").sideNav();
        $('.modal-trigger').leanModal();
        $('select').material_select();
        $('.collapsible').collapsible();

        $.validator.setDefaults({
               ignore: []
        });

        $.validator.addMethod("notEqual", function(value, element, param) {
          return value.toLowerCase() != $(param).val().toLowerCase();
        }, "Please specify a different value");

        if($('#flash_message').html().length !== 0){
            showFlash();
        }
        
        function showFlash(){
            $('#flash_message').show('fast');
            setTimeout(function() {
                $('#flash_message').fadeOut('slow');
            }, 5000);
        }

        $("#addressForm").validate({
            rules: {
                delivery_house: {
                    required: true
                },
                delivery_street: {
                    required: true
                },
                delivery_area: {
                    required: true
                }
                
            },
            submitHandler: function(form) {
                //submit the form via Ajax
                $(form).ajaxSubmit({
                    url: '/account/address', 
                    type: 'post', 
                    success: function(data){
                        //if address was added successfully 
                        console.log(data);
                        
                        //trigger modal close
                        $("#cancel_address").trigger("click");

                        //re-populate address dropdown field with newly added address
                        $.get( "/account/"+customer_id+"/address/latest", function( data ) {
                            var address = JSON.parse(data);
                            console.log(address.id);
                            var new_option = $('<option value='+address.id+'>'+address.house_bldg+' '+address.street+' '+address.village_brgy+' '+address.city+'</option>');
                            new_option.appendTo('#primary-address');
                            $('#primary-address').material_select();
                        });

                        //show success message for 5 seconds
                        $('#flash_message').html('Address successfully added!');
                        $('#flash_message').show('fast');
                        setTimeout(function() {
                            $('#flash_message').fadeOut('slow');
                        }, 5000);
                    }
                });
                $("#cancel_address").trigger("click");
            },
            errorElement : 'span',
            errorPlacement: function(error, element) {
                error.css({ 'color': '#B71C1C', 'font-size': '0.750em', 'position':'absolute', 'top':'50px' });   
                error.appendTo( element.parent() );
            }
        });

        $("#saveProfile").validate({
            rules: {
              firstname: {
                required: true
              },
              lastname: {
                required: true
              },
              contact: {
                required: true,
                minlength: 7,
                maxlength: 11
              },
              email: {
                required: true,
                email: true,
                remote: {
                    url: "/validate/"+customer_id+"/email",
                    type: "post"
                }
              },
              primary_address: {
                required: true
              }
            },
            messages: {
                firstname: "Enter your firstname",
                lastname: "Enter your lastname",
                contact: {
                    required: "Enter your contact number"
                },
                username: {
                    remote: "Username is already taken",
                    alphanumeric: "Username cannot contain special characters"
                },
                email: {
                    remote: "Email cannot be used for this account"
                },
                primary_address : {
                    required: "Please select your primary delivery address"
                }
            },
            errorElement : 'span',
            errorPlacement: function(error, element) {
                error.css({ 'color': '#B71C1C', 'font-size': '0.750em', 'position':'absolute', 'top':'50px' });   
                error.appendTo( element.parent() );
            }
        });

        $("#changePasswordForm").validate({
            rules: {
                current_password: {
                    remote: {
                        url: "/account/"+customer_id+"/password/verify",
                        type: "post"
                    },
                    required: true
                },
                new_password: {
                    required: true,
                    minlength: 6,
                    notEqual: '#current_password'
                },
                confirm_password: {
                    required: true,
                    minlength: 6,
                    equalTo: '#new_password'
                }
            },
            messages: {
                current_password: {
                    remote: "Password incorrect",
                    required: "Please enter your current password"
                },
                new_password: {
                    required: "Please enter new password",
                    notEqual: "Please enter a different password"
                },
                confirm_password: {
                    required: "Please repeat new password",
                    equalTo: "Passwords do not match"
                }
            },
            submitHandler: function(form) {
                //submit the form via Ajax
                $(form).ajaxSubmit({
                    url: '/account/password', 
                    type: 'post', 
                    success: function(data){
                        //if password was updated successfully 
                        console.log(data);
                        
                        //trigger modal close
                        $("#cancel_password").trigger("click");

                        //show success message for 5 seconds
                        $('#flash_message').html('Password successfully updated!');
                        $('#flash_message').show('fast');
                        setTimeout(function() {
                            $('#flash_message').fadeOut('slow');
                        }, 5000);
                    },
                    error: function(data ) { console.log(data); },
                });
                $("#cancel_password").trigger("click");
            },
            errorElement : 'span',
            errorPlacement: function(error, element) {
                error.css({ 'color': '#B71C1C', 'font-size': '0.750em', 'position':'absolute', 'top':'50px' });   
                error.appendTo( element.parent() );
            }
        });
    });
    
    $(function() {
        $('.btn-editprofile').click(function() {
              $('#profile'+$(this).attr('target')).toggle();
              document.getElementById("saveProfile").reset();
        });

        $('.cancel-edit').click(function() {
              $('#profile'+$(this).attr('target')).toggle();
              document.getElementById("saveProfile").reset();
        });

        $('#cancel_address').click(function(){
            document.getElementById("addressForm").reset();
        });

        $('#cancel_password').click(function(){
            document.getElementById("changePasswordForm").reset();
        });
    });
</script>
@endsection