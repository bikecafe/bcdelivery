@extends('layouts.master')

@section('content')
    <!--Sign up-->    
    <br><br>
    <div class="container">
        <div class="section">
        <!--Sign up header--> 
            <div class="row" style="margin-top:80px;">
                <div class="col s12">
                    <div id="header" class="divider"></div>
                    <h5><span class="red-text text-darken-4" style="font-weight:500;">SIGN UP</span></h5>
                    <div id="header" class="divider"></div>
                </div>
            </div>
            <div class="row">  
                <h6 class="basic-grey col s12" style="line-height:inherit;">Please ensure that all the information are correct. The company will not be liable due to incorrect information and / or non-existent recipient during time of delivery.</h6>
            </div> 
            <br>
        <!--Sign up form-->             
            <div class="row">
                <form enctype="multipart/form-data" method="post" class="col s12" id="registrationForm" >
                <!--Personal details-->
                    <div class="row">
                        <div class="col s12">
                            <div id="register" class="divider hide-on-med-and-up"></div>
                            <h5 class="blue-text text-darken-4">Personal Details</h5>   
                            <div id="register" class="divider"></div> 
                        </div>    
                    </div>
                    <div class="row">    
                        <div class="input-field col l6 s12 m6">
                            <input name="firstname" id="register_firstname" type="text">
                            <label for="register_firstname">First Name *</label>
                        </div>  
                        <div class="input-field col l6 s12 m6">
                            <input name="lastname" id="register_lastname" type="text">
                            <label for="register_lastname">Last Name *</label>
                        </div>
                    </div>
                    <div class="row">    
                        <div class="input-field col s12">
                            <input name="contact" id="register_number" type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                            <label for="register_number">Contact Number *</label>
                        </div>
                    </div>
                    <br><br>
                    <!--Delivery address--> 
                    <span class="delivery-section">
                        <div class="row">
                            <div class="col s12">
                                <div id="register" class="divider hide-on-med-and-up"></div>
                                <h5 class="blue-text text-darken-4">Delivery Address</h5>   
                                <div id="register" class="divider"></div>  
                            </div>    
                        </div>
                        <div class="row">    
                            <div class="input-field col l4 s12 m12">
                                <input name="delivery_house" id="register_street" type="text">
                                <label for="register_street">Floor / Building / House No. *</label>
                            </div>  
                            <div class="input-field col l4 s12 m12">
                                <input name="delivery_street" id="register_area" type="text">
                                <label for="register_area">Street *</label>
                            </div>
                            <div class="input-field col l4 s12 m12">
                                <select name="delivery_area"> 
                                  <option value="" disabled selected>Area / District / Barangay *</option>
                                  @foreach($delivery_area as $area)
                                  <option value="{{ $area }}">{{$area}}</option>
                                  @endforeach
                                </select>
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12">
                                <p style="color: #B71C1C !important;">**Please note that at the meantime we can only deliver to the Areas / Districts / Barangays listed on the selection above. If you wish to pick up your orders from our Café, please select "Pickup" method below.</p> 
                            </div>    
                        </div>
                    </span>
                    
                    <div class="row">
                        <div class="col s12">
                            <div id="register" class="divider hide-on-med-and-up"></div>
                            <h5 class="blue-text text-darken-4">How would you like to acquire your orders?</h5>   
                            <div id="register" class="divider"></div> 
                        </div>    
                    </div>
                    <div class="row">
                        <div class="col s12">
                            <p>
                                <input name="acquisition" type="radio" id="radio-pickup" value="1">
                                <label for="radio-pickup">Pickup</label>
                            </p>
                            <p id="acquisition_last">
                                <input name="acquisition" type="radio" id="radio-delivery" value="2" checked>
                                <label for="radio-delivery">Delivery</label>
                            </p>
                        </div>  
                    </div>
                    <div class="row">
                        <div class="col s12">
                            <p style="color: #0D47A1 !important">**You may change this option for every succeeding order.</p> 
                        </div>    
                    </div>
                    <br><br>
                    <!--Login details--> 
                    <div class="row">
                        <div class="col s12">
                            <div id="register" class="divider hide-on-med-and-up"></div>
                            <h5 class="blue-text text-darken-4">Login Details</h5>  
                            <div id="register" class="divider"></div>  
                        </div>    
                    </div>
                    <div class="row">  
                        <h6 class="basic-grey col s12" style="line-height:inherit;">Please provide a username and password below. You will use them to sign in during future visits. To protect your account, we recommend that you choose a password apart from names, birthdays, or street addresses associated with you. Sign in information is case sensitive.</h6>
                    </div>
                    <br>
                    <div class="row">    
                        <div class="input-field col s12">
                            <input name="username" id="register_username" type="text" maxlength="20">
                            <label for="register_username">Username *</label>
                        </div>
                        <div class="col s12">
                            <span class="right" style="color:#039BE5;">
                            Must be in all small caps and contain atleast 6 characters</span>
                        </div>
                    </div>      
                    <div class="row">    
                        <div class="input-field col s12">
                            <input name="email" id="register_email2" type="email">
                            <label for="register_email2" data-error="invalid!">Email *</label>
                        </div>
                    </div>
                    <div class="row">    
                        <div class="input-field col s12">
                            <input name="password" id="register_password" type="password">
                            <label for="register_password">Password *</label>
                        </div>
                        <div class="col s12">
                            <span class="right" style="color:#039BE5;">
                            Must contain atleast 6 characters</span>
                        </div>
                    </div>
                    <div class="row">    
                        <div class="input-field col s12">
                            <input name="confirm_password" id="register_password2" type="password">
                            <label for="register_password2">Confirm Password *</label>
                        </div>
                    </div> 
                    <br>
                    {!! app('captcha')->display(); !!}
                    <!--Sign up form buttons-->                   
                    <div class="row center">
                    <!--For mobile-->
                        <div class="col s12 hide-on-med-and-up">
                            <div class="row">
                            <button type="submit" id="register-button" value="submit" class="col s12 btn-large waves-effect 
                            waves-light red darken-4" style="height:64px; line-height:66px;">
                            <span style="font-weight:500;">SUBMIT</span></button>
                            </div>
                            <div class="row">
                            <a href="#!" class="col s12 btn-large waves-effect waves-light red darken-4"
                            style="height:64px; line-height:66px;">
                            <span style="font-weight:500;">CANCEL</span></a>
                            </div>
                        </div>
                    <!--For large screen-->
                        <div class="col s12 hide-on-small-only">
                            <button type="submit" id="register-button" value="submit" class="btn-large waves-effect 
                            waves-light red darken-4"><span style="font-weight:500;">SUBMIT</span></button>
                            <a href="{{ $home }}" class="btn-large waves-effect waves-light red darken-4">
                            <span style="font-weight:500;">CANCEL</span></a>
                        </div>   
                    </div>
                    <!--Sign up form buttons-->
                </form> 
            </div>
        </div>
    </div>
@endsection

@section('script')
<script src="/assets/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="/assets/js/additional-methods.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function(){
        $("input[type='radio']").click(function(){
            var radioValue = $("input[name='acquisition']:checked").val();
            if(radioValue == 1){
                $(".delivery-section").hide("fast");
            } else {
                $(".delivery-section").show("fast");
            }
        });

        $.validator.setDefaults({ 
            ignore: [],
            // any other default options and/or rules
        });
        $.validator.addMethod("lowercaseOnly", function(value, element) {
          return this.optional(element) || !/[A-Z]/.test(value); 
        }, "You are not permitted to input capital letters");
        $("#registrationForm").validate({
            rules: {
              firstname: {
                required: true
              },
              lastname: {
                required: true
              },
              contact: {
                required: true,
                minlength: 7,
                maxlength: 11
              },
              delivery_house: {
                required: function(){
                    return $("input[name='acquisition']:checked").val() == 2;
                }
              },
              delivery_street: {
                required: function(){
                    return $("input[name='acquisition']:checked").val() == 2;
                }
              },
              delivery_area: {
                required: function(){
                    return $("input[name='acquisition']:checked").val() == 2;
                }
              },
              username: {
                required: true,
                lowercaseOnly: true,
                minlength: 6,
                maxlength: 20,
                alphanumeric: true,
                remote: {
                            url: "/validate/username",
                            type: "post"
                         }
              },
              email: {
                required: true,
                email: true,
                remote: {
                    url: "/validate/email",
                    type: "post"
                }
              },
              password: {
                required: true,
                minlength: 6
              },
              confirm_password: {
                required: true,
                minlength: 6,
                equalTo: "#register_password"
              },
              'g-recaptcha-response': {
                required: function() {
                     if(grecaptcha.getResponse() == '') {
                         return true;
                     } else {
                         return false;
                     }
                 }
              }
            },
            messages: {
                firstname: "Enter your firstname",
                lastname: "Enter your lastname",
                contact: {
                    required: "Enter your contact number"
                },
                username: {
                    remote: "Username is already taken",
                    alphanumeric: "Username cannot contain special characters"
                },
                email: {
                    remote: "Email cannot be used for a new account"
                },
                confirm_password: {
                    equalTo: "Passwords do not match"
                },
                'g-recaptcha-response': {
                    required: "Recaptcha is required"
                }
            },
            errorElement : 'span',
            errorPlacement: function(error, element) {
                if (element.attr("name") == "g-recaptcha-response"){
                    error.css({ 'color': '#B71C1C', 'font-size': '0.750em', 'position':'relative', 'display':'block' });
                    error.insertAfter("div.g-recaptcha");
                } else {
                    error.css({ 'color': '#B71C1C', 'font-size': '0.750em', 'position':'absolute', 'top':'50px' }); 
                    error.appendTo( element.parent() );
                }
            }
        });
    });
</script>
@endsection