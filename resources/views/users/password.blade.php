@extends('layouts.master')

@section('content')
    <!--Log in-->    
    <br><br>
    <div class="container">
        <div class="section">
            <div class="row" style="margin-top:80px;">
                <div class="col l8 offset-l2 s12 m8 offset-m2">
                    <div class="card hoverable">
                    <br><br>
                    <!--Log in header-->
                        <div class="row">
                            <div class="col l10 offset-l1 s10 offset-s1 m10 offset-m1">
                                <h5><span class="red-text text-darken-4" style="font-weight:500;">Enter new password</h5>
                                <div id="header" class="divider"></div>
                            </div>
                        </div>
                        <!--Log in form-->           
                        <div class="row">
                            <form enctype="multipart/form-data" method="post" class="col l10 offset-l1 s10 offset-s1 m10 offset-m1" id="login-form">
                                <div class="row">    
                                    <div class="input-field col s12">
                                        <input id="login_password" type="password" name="password">
                                        <label for="login_password">New Password</label>
                                    </div>
                                </div>
                                <div class="row">    
                                    <div class="input-field col s12">
                                        <input id="login_password2" type="password" name="confirm_password">
                                        <label for="login_password2">Confirm New Password</label>
                                    </div>
                                </div>
                                @if(Session::has('error'))
                                <div class="row">
                                {!! Session::get('error') !!}
                                </div>
                                @endif
                                <br><br>    
                                <input type="hidden" name="uid" value="{{Session::get('uid')}}">
                                <div class="row center">
                                    <div class="col s12">
                                        <button type="submit" id="login-button" value="submit" class="btn-large waves-effect 
                                        waves-light red darken-4"><span style="font-weight:500;">SUBMIT</span></button>
                                    </div>
                                </div>
                                <br>
                            </form>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
    <br>    
@endsection

@section('script')
<script src="/assets/js/jquery.validate.min.js" type="text/javascript"></script>
<script>

    $("#login-form").validate({
        rules: {
          password: {
            required: true,
            minlength: 6
          },
          confirm_password: {
            required: true,
            minlength: 6,
            equalTo: '#login_password'
          }
        },
        messages: {
            password: {
                required: "Enter new password"
            },
            confirm_password: {
                required: "Enter password again",
                equalTo: "Passwords do not match"
            }
        },
        errorElement : 'span',
        errorPlacement: function(error, element) {
            error.css({ 'color': '#B71C1C', 'font-size': '0.750em', 'position':'absolute', 'top':'50px' });   
            error.appendTo( element.parent() );
        }
    });
</script>
@endsection