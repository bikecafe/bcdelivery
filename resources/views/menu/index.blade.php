@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="section">
        <br><br>
            <div class="row" style="margin-top:80px;">
                <div class="col s12">
                    <div id="header" class="divider"></div>
                    <h5><span class="red-text text-darken-4" style="font-weight:500;">ROUTE 88 MENU</span></h5>
                    <div id="header" class="divider"></div>
                </div>
            </div>
        <!--Menu nav-->
            <div class="row">
                <div class="col l3 s12 m12">
                <!--Menu nav: large-->
                    <ul id="menu-id" class="menu-nav hide-on-med-and-down">
                        <?php $x = 0; ?>
                        @foreach($productList as $category)
                            <li class="{{ $x == 0 ? 'active':''}}"><a class="menu-item {{ $x == 0 ? 'active':''}}" target="{{$category['id']}}">
                            <i id="menu-i" class="material-icons left">remove</i>{{$category['name']}}</a></li>
                            <?php $x++; ?>
                        @endforeach
                    </ul>
                <!--Menu nav: med & down-->      
                    <ul id="menu=id" class="menu-nav hide-on-large-only">
                        <li class="divider menu-divider"></li>
                        @foreach($productList as $category)
                            <li><a class="menu-item" target="{{$category['id']}}">
                        <i id="menu-i" class="material-icons left">remove</i>{{$category['name']}}</a></li>
                        @endforeach
                    </ul>   
                </div>
                <!--Menu content-->
                <div class="col l8 offset-l1 s12 m12">
                <!--first content-->
                    @foreach($productList as $key => $category)
                        <div id="menu{{$category['id']}}" class="hidden targetMenu">
                            <div class="row left-align">
                                <h5 class=" col s12 red-text text-darken-4" style="font-weight:500;">
                                <i class="material-icons left">restaurant</i>{{$category['name']}}</h5>  
                            </div>
                            <div class="row">
                                @foreach($category['products'] as $product)
                                <div class="col l6 s12 m6">
                                    <div class="card hoverable">
                                        <div class="card-image waves-effect waves-block waves-light">
                                            <img class="activator" src="{{ Helper::getSettingValue('admin_url') ? Helper::getSettingValue('admin_url') : 'http://bcafe.dev/uploads'}}/{{$product['image']}}">
                                        </div>
                                        <div class="card-content">
                                            <div class="basic-price">₱ {{$product['price']}}
                                            <span class="activator basic-morevertical">
                                            <i class="material-icons right">more_vert</i></span><br>
                                            <span class="red-text text-darken-3 product-name">{{$product['name']}}</span></div>
                                            <br>
                                            <div class="divider"></div>
                                            <br>
                                            <div class="center">
                                                <a href="#add-tray" class="btn-large basic-add-tray waves-effect 
                                                waves-light blue darken-4 modal-trigger add-to-tray" code="{{$product['id']}}" code-name="{{$product['name']}}"><i class="material-icons left">add_circle_outline</i>
                                                Add to Tray</a>
                                            </div>
                                        </div>
                                        <div class="card-reveal">
                                            <span class="card-title"><span class="blue-text text-darken-4 basic-revealtitle">
                                            {{$product['name']}}</span>
                                            <i class="material-icons right close-icon">close</i></span>
                                            <p>{{$product['description']}}</p>
                                        </div>  
                                    </div>         
                                </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <!--Add to Tray Modal-->
    <div id="add-tray" class="modal">
        <div class="modal-content">
            <br>
            <div class="row">
                <div class="col l10 offset-l1 col s10 offset-s1 col m10 offset-m1">
                    <h5 id="modal_product_name" class="red-text text-darken-4" style="font-weight:500;"></h5>
                    <div id="header" class="divider"></div>
                </div>
            </div>
            <div class="row">
                <form id="addTrayForm">
                    <br/>
                    <input type="hidden" id="prod_id" name="id" value=""/>
                    <div class="row center">
                        <label for="prod_qty" style="font-size:1.1rem">Order Quantity</label>
                        <input id="prod_qty" name="quantity" type="number" min="1" max="20" value="1" style="display:block;width:100px;margin:0 auto;font-size:16px" />
                    </div>
                    <div class="row center">
                        <div class="col l10 offset-l1 col s10 offset-s1 col m10 offset-m1">
                            <span style="text-align:center; color:#DE0A16;font-size:12px"><b>Note:</b> Product is subject to availability upon ordering. <br/>For bulk orders, please call us at (02) 433 5368.</span>
                        </div>
                    </div>
                    <div class="row center">
                        <button type="submit" class="btn waves-effect waves-light red darken-4">
                                    ADD TO TRAY</button>
                        <a href="#!" id="cancel_tray" class="btn waves-effect waves-light red darken-4 modal-close">
                                    CANCEL</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="/assets/js/jquery.form.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        
        $(document).ready( function () {
            $(".button-collapse").sideNav();

            $('.basic-add-tray').click(function(){
                var product_name = $(this).parents('.card-content').find('.product-name').html();
                $("#modal_product_name").html(product_name.toUpperCase());
                $("#prod_id").val($(this).attr('code'));
                $("#add-tray").openModal();            
            });

            $("#addTrayForm").submit(function(e){
                e.preventDefault();
                $('#addTrayForm').ajaxSubmit({
                  type: "POST",
                  url: "tray/add",
                  dataType: 'json',
                  success: function(data){
                        $("#cancel_tray").trigger("click");
                        document.getElementById("addTrayForm").reset();
                        //if add to tray successful
                        if(data.status == 1){
                            console.log(data.added);
                            //show success message for 5 seconds
                            $('#flash_message').html('Order successfully added!');
                            $('#flash_message').show('fast');
                            setTimeout(function() {
                                $('#flash_message').fadeOut('slow');
                            }, 2000);
                            //update tray count
                            $.get( "/tray/count", function( data ) {
                                if(parseInt(data) > 0){
                                    $("#tray_count").html(data);
                                    $("#mobile_cart_total").html("("+data+")");
                                } else {
                                    $("#tray_count").html("");
                                    $("#mobile_cart_total").html("");
                                }
                                console.log('tray:'+data);
                            });
                        }
                  },
                  error: function(xHr, textStatus, errorThrown) {
                     console.log(xHr.responseText);
                     console.log(errorThrown);
                  }
                });
            });
        });
        
        $(function() {
            $('.menu-item').click(function() {
                  $('.targetMenu').hide();
                  $('#menu'+$(this).attr('target')).removeClass('.hidden');
                  $('#menu'+$(this).attr('target')).show();
                  $('.menu-item').removeClass('active');
                  $(this).addClass('active');
            });
        });
        
        var selector = '.menu-nav li';
        $(selector).on('click', function(){
        $(selector).removeClass('active');
        $(this).addClass('active');
        });

        initial();
 
        function initial(){
            $(".menu-item.active").each(function(){
                console.log($(this).attr('target'));
                $('.targetMenu').hide();
                $('#menu'+$(this).attr('target')).removeClass('.hidden');
                  $('#menu'+$(this).attr('target')).show();
                  $('.menu-item').removeClass('active');
                  $(this).addClass('active');
            });
        }
    </script>

@endsection